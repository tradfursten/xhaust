from gpxParse import importGPX, convertUTM, runLength, runTime, runPace
from jchart import Chart
import json
import math

filename = 'RunnerUp_0057.gpx'
pointList = importGPX(filename)
convertUTM(pointList)
len(pointList)

def splits(points):
    label = 1
    cumulativeDist = 0.0
    initTime = points[0][2]
    splits={}
    times = []
    for i in range(1,len(points)):
        deltaX = abs(points[i][0] - points[i-1][0])
        deltaY = abs(points[i][1] - points[i-1][1])
        deltaDist = math.sqrt((deltaX*deltaX)+(deltaY*deltaY))
        cumulativeDist+=deltaDist
        if cumulativeDist >= 1000:
            deltaTime = points[i][2]-initTime
            deltaTimeMin = int((points[i][2] - initTime)/60)
            deltaTimeSec = int((points[i][2] - initTime)%60)
            print(str(cumulativeDist), "-- km # " +str(label),str(deltaTimeMin)+":"+str(deltaTimeSec))
            initTime = points[i][2]
            cumulativeDist = 0.0
            times.append(deltaTime)
            label+=1
            print(times)
    splits['data']=times
    print(splits)
    return splits

splits = splits(pointList)
print(splits)

class LineChart(Chart):
chart_type = 'line'
def get_datasets(self, **kwargs):
    return [{
        'label': "Splits",
        'data': splits[data]
    }]
