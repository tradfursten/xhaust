from django.contrib import admin

from .models import User, Cycle, Hike, Run, Statistic

# Register your models here.

admin.site.register(User)
admin.site.register(Run)
admin.site.register(Cycle)
admin.site.register(Hike)
admin.site.register(Statistic)
