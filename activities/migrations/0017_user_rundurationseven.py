# Generated by Django 2.1 on 2018-09-02 16:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('activities', '0016_auto_20180902_1640'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='rundurationseven',
            field=models.FloatField(default=0.0),
        ),
    ]
