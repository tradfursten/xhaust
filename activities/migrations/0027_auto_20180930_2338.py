# Generated by Django 2.1 on 2018-09-30 23:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('activities', '0026_auto_20180930_2151'),
    ]

    operations = [
        migrations.AddField(
            model_name='statistic',
            name='runduration30',
            field=models.FloatField(default=0.0),
        ),
        migrations.AddField(
            model_name='statistic',
            name='runduration7',
            field=models.FloatField(default=0.0),
        ),
        migrations.AlterField(
            model_name='statistic',
            name='runpace7',
            field=models.FloatField(default=0.0, max_length=5),
        ),
    ]
