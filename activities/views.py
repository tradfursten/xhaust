from activities.gpxParse import importGPX, convertUTM, runLength, runTime, runPace
from activities.models import User, Run, Cycle, Hike, Statistic

from . import charts

from bs4 import BeautifulSoup
from datetime import datetime
from django_ajax.decorators import ajax
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.http import HttpResponse, JsonResponse
from django.template import loader
from django.shortcuts import render, redirect
from django.views.generic.edit import CreateView
from .activity_sorter import feedAll
from .forms import RunForm, SlideForm, CommentForm, DataForm, PhotoForm, DatePicker, ReplyForm, GPSForm, MarkdownForm
from .models import Run, User, Statistic
from .gpxParse import importGPX, convertUTM, runLength, runTime, runPace, importTCX
from .img_scale import downscale
from jchart import Chart
from markdown2 import Markdown
from social_django.models import UserSocialAuth
from steem import Steem
from steemconnect.operations import Vote, Comment, CommentOptions
from steemconnect.client import Client
from steem.converter import Converter
from steem.post import Post

import humanize
import json
import magic
import math
import re
import requests
import tcxparser
from .secret import *

# Create your views here.

class SignUpView(CreateView):
    template_name = 'signup.html'
    form_class = UserCreationForm

def calc_votevalue():
#    weight = request.GET.get('weight', None)
    data = {
        'weight': 30,
        'username': 'mstafford',
    }
    return JsonResponse(data)

def index(request):
    template = loader.get_template('activities/index.html', using='backend')
    good_reads = {}
    blog_posts = {}
    s = Steem()
#    blogroll = s.get_blog('exhaust',-1,10)             #fixing shit for mark --john
    blogroll = s.get_blog('exhaust',-1,20)
    readcounter = 0
    blogcounter = 0
    userList = User.objects.all()
    if len(userList) > 0:
        activityleader = ['null',0] #[username, count, post-title, post-link]
        distanceleader = ['null',0] #[username, distance, post-title, post-link]
        paceleader = ['null',100] #[username, pace, post-title, post-link]
        ## Activity Counter
        if len(Statistic.objects.all()) > 0:
            countleader = list(reversed(Statistic.objects.order_by('activitycount7')))[:1]
            user = countleader[0].user
            activity = list(Run.objects.filter(user=user))[-1]
            activityleader = [user.username, countleader[0].activitycount7, activity.title, activity.id]

            distleader = list(reversed(Statistic.objects.order_by('rundistance7')))[:1]
            user = distleader[0].user
            activity = list(Run.objects.filter(user=user))[-1]
            distanceleader = [user.username, round(distleader[0].rundistance7,2), activity.title, activity.id]

            speedleader = Statistic.objects.order_by('runpace7')
            speedleader = list(speedleader.exclude(runpace7=0))
            user = speedleader[0].user
            activity = list(Run.objects.filter(user=user))[-1]
            paceleader = [user.username, round(speedleader[0].runpace7,2), activity.title, activity.id]
    else:
        activityleader = ['SpaceGhost','69','SpaceGhosting Coast2Coast','69']
        distanceleader = ['SpaceGhost','69','SpaceGhosting Coast2Coast','69']
        paceleader = ['SpaceGhost','69','SpaceGhosting Coast2Coast','69']

    for post in blogroll:
        comment = post['comment']
        author = comment['author']
        if comment['json_metadata']:
            json_metadata = json.loads(comment['json_metadata'])
        if author == 'exhaust': ## Populate blog posts at bottom of page with articles written by @RB
            img_link = re.search('!\[.{0,30}]\(.{10,120}\)',comment['body'])
            if img_link:
                img_link = re.sub('!\[.{0,30}]\(','',img_link[0])
                img_link = re.sub('\)','',img_link)
            blog_posts[blogcounter]={}
            blog_posts[blogcounter]['created']=humanize.naturaldelta(datetime.strptime(comment['created'],'%Y-%m-%dT%H:%M:%S'))
            blog_posts[blogcounter]['title']=comment['title']
            blog_posts[blogcounter]['preview']=comment['body'][:280]
            blog_posts[blogcounter]['author']='exhaust'
            blog_posts[blogcounter]['body']=comment['body']
            blog_posts[blogcounter]['imglink']=img_link
            blog_posts[blogcounter]['permlink']=comment['permlink']
            blogcounter += 1
        if author != 'exhaust' and author != '':
            img_link = re.search('!\[.{0,256}]\(.{10,256}\)',comment['body'])
            if img_link:
                img_link = re.sub('!\[.{0,256}]\(','',img_link[0])
                img_link = re.sub('\)','',img_link)
                img_link = re.sub('https://steemitimages.com/\d{1,3}x\d{1,3}/','',img_link)
                if 'steepshot' in json_metadata['app']: ## Ensure that steepshot posts can be resteemed nicely
                    img_link = json_metadata['image']
                    img_link = img_link[0]
            else:
                img_link = ''
            good_reads[readcounter]={} ## Populate front-page articles w/ resteemed blog posts
            good_reads[readcounter]['created']=humanize.naturaldelta(datetime.strptime(comment['created'],'%Y-%m-%dT%H:%M:%S'))
            good_reads[readcounter]['title']=comment['title']
            good_reads[readcounter]['preview']=comment['body'][:140]
            good_reads[readcounter]['body']=comment['body']
            good_reads[readcounter]['author']=author
            good_reads[readcounter]['imglink']=img_link
            good_reads[readcounter]['permlink']=comment['permlink']
            readcounter +=1
            if readcounter >= 20:
                break

    if str(request.user) != 'AnonymousUser':
        user = (UserSocialAuth.objects.get(user=request.user))
        context = {
            'STEEM_ID': user.uid,
            'GOOD_READS': good_reads,
            'BLOG': blog_posts,
            'ACTIVITYLEADER': activityleader,
            'DISTANCELEADER': distanceleader,
            'PACELEADER': paceleader,
        }
    else:
        context = {
            'GOOD_READS': good_reads,
            'BLOG': blog_posts,
            'ACTIVITYLEADER': activityleader,
            'DISTANCELEADER': distanceleader,
            'PACELEADER': paceleader,
        }
    return HttpResponse(template.render(context,request))

def testfloppy(request):
    template = loader.get_template('markdown.html', using='django')
    if request.method == 'POST':
        votewt = MarkdownForm(request.POST)
        if votewt.is_valid():
            weight = votewt['weight']
            context = {
                'WEIGHT':weight,
                'SLIDERFORM':votewt,
                }
            return redirect('/test/'+str(weight.value()))
    else:
        votewt = MarkdownForm()
        context = {
            'SLIDERFORM':votewt,
            }
    return HttpResponse(template.render(context,request))

def testweight(request, weight):
    return HttpResponse("Vote weight = " + str(weight) + "%")

def article(request, steem_id, link):
    post = Post('@'+steem_id+'/'+link)
    voterList = []
    voted = False
    user = 'null'
    plink = '@'+steem_id+'/'+link
    try:
        user = UserSocialAuth.objects.get(user=request.user)
    except:
        user = 'null'
    for voter in post['active_votes']:
        voterList.append(voter['voter'])
        if str(user) in voterList:
            voted = True
    replies = list(Post.get_replies(post))
    if len(replies)>=1:
        comments = getReplies(replies, user)
    else:
        comments = []
    markdowner = Markdown()
    postBody = markdowner.convert(post.body)
    soup = BeautifulSoup(postBody, 'html.parser')
    images = soup.find_all('img')
    for image in images:
        image['class']='responsive'
    postBody = soup
    template = loader.get_template('activities/post_full.html', using='backend')
    if request.method == 'POST':
        commenter = CommentForm(request.POST)
        if commenter.is_valid():
            body = commenter.cleaned_data['description']
            createComment(request, str(post.author),str(plink), str(body))
    else:
        commenter = CommentForm()

    context = {
        'AUTHOR':post.author,
        'BODY':postBody,
        'TITLE':post.title,
        'PERMLINK': link,
        'COMMENTS':comments,
        'COMMENTER':commenter,
        'VOTED':voted,
    }
    return HttpResponse(template.render(context,request))

def leaderboard(request):
    s = Steem()
    template = loader.get_template('activities/leaderboard.html', using='backend')
    leaderboards = Statistic.objects.all()
    leaderboards = leaderboards.order_by('rundistance7').reverse().exclude(activitycount7=0)
    context = {}
    USERS = {}
    for user in leaderboards:
        USERS[str(user.user)]=[str(user.user),str(user.activitycount7),str(round(user.rundistance7,2)),str(round(user.runduration7,2)),str(round(user.runpace7,2))]
    context = {
        'USERS':USERS
    }
    return HttpResponse(template.render(context,request))

def dash(request):
    s = Steem()
    c = Converter()
    template = loader.get_template('activities/dash.html', using='backend')
    user = UserSocialAuth.objects.get(user=request.user)
    account_info = s.get_account(user.uid)
    followerList = []
    followingList = []
    friendList = []
    followers = s.get_followers(user.uid,-1,'blog',1000)
    followings = s.get_following(user.uid, -1, 'blog', 1000)
    for follow in followers:
        followerList.append(follow['follower'])
    for follow in followings:
        followingList.append(follow['following'])
    for follow in followingList:
        if follow in followerList:
            friendList.append(follow)
    if account_info['json_metadata'] != '':
        json_data = json.loads(account_info['json_metadata'])
    else:
        json_data = {}
        json_data['profile']={}
        json_data['profile']['about']='EXHAUSTed'
    ## Need to check if 'about' key exists
    if 'about' not in json_data['profile']:
        json_data['profile']['about'] = 'EXHAUSTed'
    ## Some activity information below:
    dbUser = User.objects.get(username = user.uid)
    runList = Run.objects.filter(user=dbUser)
    runList = list(reversed(runList))
    runCount = len(runList)
    totalDist = 0
    totalTime = 0
    totalReward = 0
    for run in runList:
        totalDist += run.distance
        totalTime += run.duration
        totalReward += run.rewards
    steempower = round(c.vests_to_sp(float(re.sub('VESTS','',(account_info['vesting_shares'])))),3)
    context = {
        'STEEM_ID': user.uid,
        'SBD_BAL': account_info['sbd_balance'],
        'STEEM_BAL': account_info['balance'],
        'STEEM_POW': str(steempower),
        'ABOUT': json_data['profile']['about'],
        'RUN_COUNT': runCount,
        'RUN_LIST': runList,
        'TOTAL_DIST': totalDist/1000,
        'TOTAL_HRS': int(totalTime/3600),
        'TOTAL_MINS': int(totalTime%3600/60),
        'TOTAL_SECS': totalTime%60,
        'TOTAL_REWARD': totalReward,
        'FOLLOWERS': len(followerList),
        'FOLLOWING': len(followingList),
        'FRIENDS': len(friendList),
    }
    return HttpResponse(template.render(context,request))

def feed(request, activity_type, sort):
    s = Steem()
    template = loader.get_template('activities/feed.html', using='django')
    sliderform = SlideForm()
    try:
        user = UserSocialAuth.objects.get(user=request.user)
        followlist = s.get_following(str(user),-1,'blog',1000)
        friendList = []
        for friend in followlist:
            friendList.append(friend['following'])
    except:
        user = 'null'
    if activity_type == 'run':
        activityList = Run.objects.filter(posted=True)
        activityList = list(reversed(activityList))
    if activity_type == 'cycle':
        activityList = Cycle.objects.filter(posted=True)
        activityList = list(reversed(activityList))
    if activity_type == 'hike':
        activityList = Hike.objects.filter(posted=True)
        activityList = list(reversed(activityList))
    if activity_type == 'all':
        activityList = feedAll()
    activityList = activityList[:20]
    myFeed = {}
    for activity in activityList:
        post = Post(activity.permlink)
        myFeed[activity]=[]
        activity.reward = post.reward
        activity.duration = int(activity.duration/60)
        activity.distance = activity.distance/1000
        activity.inpast = humanize.naturaldelta(post.created)
        for voter in post['active_votes']:
            myFeed[activity].append(voter['voter'])
            if str(user) in myFeed[activity]:
                activity.voted = True
    context = {
        "RUNLIST":activityList,
        "FEEDLIST":myFeed,
        "SLIDERFORM":sliderform,
        "ACTIVITY_TYPE":activity_type,
        "SORT_TYPE":sort,
        }
    return HttpResponse(template.render(context,request))

def browse(request, steem_id):
    s = Steem()
    template = loader.get_template('activities/left-sidebar.html', using='backend')
    user = steem_id
    context = {}
    account = s.get_account(user)
        
    profile_image = "TEMPLATE IMAGE"
    profile_about = ""
    profile_location = ""
    profile_banner = ""
    recentActivities = []
    recentPosts = []
    postList = s.get_blog(str(user),-1,30)
    postList = list(reversed(postList))
    postList = postList[:int(len(postList)/2)]
    postList = list(reversed(postList))
    newList = []
    for post in postList:
        json_data = json.loads(post['comment']['json_metadata'])
        if json_data['app']=='exhaust/0.1a' or post['comment']['author']!=str(user):
            print("Not adding post: " + str(post['comment']['title']))
        else:
            print("Adding post: " + str(post['comment']['title']))
            newList.append([post,humanize.naturaldelta(datetime.strptime(str(post['comment']['created']),'%Y-%m-%dT%H:%M:%S'))])
    newList = newList[:5]
    runList = {}
    context = {
        'profile_image': "https://openclipart.org/download/236194/1452091957.svg",
        'about': "My profile ain't none of yo business",
        'location': "UNDISCLOSED",
        'cover_image': "null",
    }
    users = User.objects.all()
    userList = []
    for name in users:
        userList.append(name.username)
    if user in userList:
        username = User.objects.get(username=user)
        runList = Run.objects.filter(user=username)
        runList = list(reversed(runList))[:5]
    else:
        runList="No Activities Yet!"
    if 'profile' in account['json_metadata']:
        account_info = json.loads(account['json_metadata'])
        for field in context:
            if field in account_info['profile']:
                context[field] = account_info['profile'][field]
    context['STEEM_ID'] = user
    context['RUNLIST']=runList
    context['POSTLIST']=newList
    context['NUMPOSTS']=len(newList)
    return HttpResponse(template.render(context,request))

@login_required
def test_new_upload(request, activity_type):
    user = UserSocialAuth.objects.get(user=request.user)
    steem_id = user.uid
    account_id = User.objects.get(username = steem_id)
    template=loader.get_template('test.html',using='backend')
    splits = []
    activityList = {
        'run':{'NICK':'Run','PATH':"documents/runs/"},
        'cycle':{'NICK':'Ride','PATH':"documents/cycles/"},
        'hike':{'NICK':'Hike', 'PATH':"documents/hikes/"},
        'climb':{'NICK':'Climb', 'PATH':"documents/climbs/"},
        'surf':'Session',
        }
    if request.method == 'POST':
        if 'gpsfile' in request.FILES:
            if activity_type == 'run':
                activity=Run(user=account_id, title=request.POST['title'], comments=request.POST['description'], gpsfile=request.FILES['gpsfile'],uploaded_at=datetime.now())
                activity.save()
            if activity_type == 'cycle':
                activity=Cycle(user=account_id, title=request.POST['title'], comments=request.POST['description'], gpsfile=request.FILES['gpsfile'],uploaded_at=datetime.now())
                activity.save()
            if activity_type == 'hike':
                activity=Hike(user=account_id, title=request.POST['title'], comments=request.POST['description'], gpsfile=request.FILES['gpsfile'],uploaded_at=datetime.now())
                activity.save()
            if 'photo1' in request.FILES:
                activity.photo = request.FILES['photo1']
                activity.save()
                downscale(
                    str(activity.photo.name),
                    activityList[activity_type]['PATH'],
                    str(account_id)+"-"+str(activity.pk)
                )
                if '.gif' in activity.photo.name:
                    pass
                else:
                    activity.photo = activityList[activity_type]['PATH']+str(account_id)+"-"+str(activity.pk)+".jpg"
                    activity.save()
            else:
                activity.photo = activity_type + '-default.jpg'
                activity.save()
            if 'photo2' in request.FILES:
                activity.photo2 = request.FILES['photo2']
                activity.save()
                downscale(
                    str(activity.photo2.name),
                    activityList[activity_type]['PATH'],
                    str(account_id)+"-2-"+str(activity.pk)
                )
                if '.gif' in activity.photo.name:
                    pass
                else:
                    activity.photo2 = activityList[activity_type]['PATH']+str(account_id)+"-2-"+str(activity.pk)+".jpg"
                    activity.save()
            if 'photo3' in request.FILES:
                activity.photo3 = request.FILES['photo3']
                activity.save()
                downscale(
                    str(activity.photo3.name),
                    activityList[activity_type]['PATH'],
                    str(account_id)+"-3-"+str(activity.pk)
                )
                if '.gif' in activity.photo3.name:
                    pass
                else:
                    activity.photo3 = activityList[activity_type]['PATH']+str(account_id)+"-3-"+str(activity.pk)+".jpg"
                    activity.save()
            pointList = []
            if "gpx" in str(activity.gpsfile.name):
                distance, duration, pace, points, times, splits = importGPX('media/'+str(activity.gpsfile.name))
                activity.distance = distance
                activity.duration = duration
                activity.pace = pace
                activity.save()
                pointList.clear()
            if "tcx" in str(activity.gpsfile.name):
                avgHR = 0
                maxHR = 0
                points, avgpace, avgHR, maxHR, ascent, completed, distance, duration, splits = importTCX('media/'+str(activity.gpsfile.name))
                activity.distance = distance
                activity.duration = duration
                activity.save()
        else:
            timer = re.findall('\d{1,2}',request.POST['duration'])
            duration = 0
            mult = 2
            for time in timer:
                duration += int(time) * int(math.pow(60,int(mult)))
                mult -= 1
            if activity_type == 'run':
                if request.FILES:
                    activity=Run(user=account_id, title=request.POST['title'], comments=request.POST['description'],uploaded_at=datetime.now(), distance=int(float(request.POST['distance'])*1000), duration=duration)
                    activity.pace = str((activity.duration/60)/(activity.distance/1000))
                    activity.save()
                    if 'photo1' in request.FILES:
                        activity.photo = request.FILES['photo1']
                        activity.save()
                    if 'photo2' in request.FILES:
                        activity.photo2 = request.FILES['photo2']
                        activity.save()
                    if 'photo3' in request.FILES:
                        activity.photo3 = request.FILES['photo3']
                        activity.save()
                else:
                    activity=Run(user=account_id, title=request.POST['title'], comments=request.POST['description'], uploaded_at=datetime.now(), distance=int(float(request.POST['distance'])*1000), duration=duration)

            if activity_type == 'cycle':
                if request.FILES:
                    activity=Cycle(user=account_id, title=request.POST['title'], comments=request.POST['description'],uploaded_at=datetime.now(), distance=int(float(request.POST['distance'])*1000), duration=duration)
                    activity.pace = str((activity.duration/60)/(activity.distance/1000))
                    activity.save()
                    if 'photo1' in request.FILES:
                        activity.photo = request.FILES['photo1']
                        activity.save()
                    if 'photo2' in request.FILES:
                        activity.photo2 = request.FILES['photo2']
                        activity.save()
                    if 'photo3' in request.FILES:
                        activity.photo3 = request.FILES['photo3']
                        activity.save()
                else:
                    activity=Cycle(user=account_id, title=request.POST['title'], comments=request.POST['description'], uploaded_at=datetime.now(), distance=int(float(request.POST['distance'])*1000), duration=duration)

            if activity_type == 'hike':
                if request.FILES:
                    activity=Hike(user=account_id, title=request.POST['title'], comments=request.POST['description'],uploaded_at=datetime.now(), distance=int(float(request.POST['distance'])*1000), duration=duration)
                    activity.pace = str((activity.duration/60)/(activity.distance/1000))
                    activity.save()
                    if 'photo1' in request.FILES:
                        activity.photo = request.FILES['photo1']
                        activity.save()
                    if 'photo2' in request.FILES:
                        activity.photo2 = request.FILES['photo2']
                        activity.save()
                    if 'photo3' in request.FILES:
                        activity.photo3 = request.FILES['photo3']
                        activity.save()
                else:
                    activity=Hike(user=account_id, title=request.POST['title'], comments=request.POST['description'], uploaded_at=datetime.now(), distance=int(float(request.POST['distance'])*1000), duration=duration)

            activity.save()
            if activity.photo:
                downscale(
                    str(activity.photo.name),
                    activityList[activity_type]['PATH'],
                    str(account_id)+"-"+str(activity.pk)
                )
                if '.gif' in activity.photo.name:
                    pass
                else:
                    activity.photo = activityList[activity_type]['PATH']+str(account_id)+"-"+str(activity.pk)+".jpg"
                    activity.save()
            if activity.photo2:
                downscale(
                    str(activity.photo2.name),
                    activityList[activity_type]['PATH'],
                    str(account_id)+"-"+str(activity.pk)
                )
                if '.gif' in activity.photo2.name:
                    pass
                else:
                    activity.photo2 = activityList[activity_type]['PATH']+str(account_id)+"-"+str(activity.pk)+".jpg"
                    activity.save()
            if activity.photo3:
                downscale(
                    str(activity.photo3.name),
                    activityList[activity_type]['PATH'],
                    str(account_id)+"-"+str(activity.pk)
                )
                if '.gif' in activity.photo3.name:
                    pass
                else:
                    activity.photo3 = activityList[activity_type]['PATH']+str(account_id)+"-"+str(activity.pk)+".jpg"
                    activity.save()
        return redirect('/dash/'+activity_type+'/'+str(activity.pk)+'/')
    else:
        dataform = DataForm()
        photoform = PhotoForm()
        commentform = CommentForm()
        datepicker = DatePicker()
        gpsform = GPSForm()
    context = {
        'dataform':dataform,
        'photoform':photoform,
        'commentform':commentform,
        'datepicker':datepicker,
        'gpsform': gpsform,
        'ACTIVITY': activityList[activity_type]['NICK'],
    }

    return HttpResponse(template.render(context,request))

def getReplies(replies, user):
    comments = {}
    voterList = []
    markdowner = Markdown()
    counter = 0
    for comment in replies:
        subcomments = {}
        voted = False
        votes = 0
        commentpost = Post('@'+comment.author+'/'+comment.permlink)
        for voter in commentpost['active_votes']:
            voterList.append(voter['voter'])
        if str(user) in voterList:
            voted = True
        votes = len(voterList)
        subs = list(Post.get_replies(comment))
        for sub in subs:
            subvoterList = []
            subvoted = False
            for voter in sub['active_votes']:
                subvoterList.append(voter['voter'])
            if str(user) in subvoterList:
                subvoted = True
            subvotes = len(subvoterList)
            subcomments[counter] = {
                'AUTHOR': sub.author,
                'BODY': markdowner.convert(sub.body),
                'PERMLINK': sub.permlink,
                'REWARD': sub.reward,
                'VOTES': subvotes,
                'VOTED': subvoted,
            }
        comments[counter] = {
            'AUTHOR': comment.author,
            'BODY': markdowner.convert(comment.body),
            'PERMLINK': comment.permlink,
            'REWARD': comment.reward,
            'VOTES': votes,
            'VOTED': voted,
            'REPLIES': subcomments,
            'NUMREPLIES': len(subcomments),
        }

        voterList.clear()
        counter += 1
        ### NEED TO ADD SUPPORT FOR SUB-COMMENTS ###
    return(comments)

def gps_detail(request, activity_type, activity_id):
    template = loader.get_template('activities/run_detail.html', using='backend')
    try:
        user = UserSocialAuth.objects.get(user=request.user)
    except:
        user = 'null'
    if activity_type == 'run':
        activity=Run.objects.get(pk=activity_id)
    if activity_type == 'cycle':
        activity=Cycle.objects.get(pk=activity_id)
    if activity_type == 'hike':
        activity = Hike.objects.get(pk=activity_id)
    markdowner = Markdown()
    reward = 0.0
    voterList = []
    if activity.posted == True:
        post = Post(activity.permlink)
        replies = list(Post.get_replies(post))
        if len(replies)>=1:
            comments = getReplies(replies, user)
        else:
            comments = []
        reward = post.reward
        activity.rewards = float(re.sub(' SBD','',str(reward)))
        activity.save()
        for voter in post['active_votes']:
            voterList.append(voter['voter'])
        activity.votes = len(voterList)
        activity.voters = voterList
        try:
            if str(user) in voterList:
                run.voted = True
        except:
            pass
    else:
        comments = {}
    if activity.gpsfile:
        filename = 'media/'+str(activity.gpsfile.name)
    else:
        filename =  'null'
    param = 0
    splitDist = 1000
    smoothDist = 50
    distTotal = 0.0
    sample=5
    splitCurr = 0.0
    elev = []
    pace = []
    heart = []
    distance = 0.0
    heartchart = []
    if activity_type == 'run':
        avgpace = activity.pace
    maxHR = 0
    avgHR = 0
    ascent = 0
    completed=''
    splits = []
    if activity.gpsfile: 
        if 'gpx' in filename:
            print("GPX File Format Detected")
            distance, duration, pacecalc, points, timestart, splits = importGPX(filename)
            initTime = timestart.timestamp()
            minutes = str(int(pacecalc)) 
            seconds = (int((pacecalc%1)*60))
            seconds = format(seconds, '02d')
            avgpace = minutes+':'+seconds
            activity.pace = avgpace
            activity.save()
            avgHR = 'NO HR DATA'
            maxHR = 'NO HR DATA'
            for i in range(1, len(points)):
                splitCurr += (points[i]).distance_from_start-points[i-1].distance_from_start
                if splitCurr >= smoothDist:
                    distance = round((points[i].distance_from_start)/1000,2)
                    pace.append([distance, ((points[i].point.time.timestamp()-initTime)/60)/distance])
                    elev.append([distance, (points[i].point.elevation)])
                    splitCurr = 0.0
        if 'tcx' in filename:
            print("TCX File Format Detected")
            points, avgpace, avgHR, maxHR, ascent, completed, length, duration, splits = importTCX(filename)
            activity.pace = avgpace
            activity.save()
            for i in range(1,len(points)):
                splitCurr += (points[i][0]-points[i-1][0])
                if splitCurr >= smoothDist:
                    distance = round((points[i][0])/1000,2)
                    pace.append([distance, ((points[i][1]-points[i-smoothDist][1])/60)/((points[i][0]-points[i-smoothDist][0])/1000)])
                    elev.append([distance, points[i][2]])
                    heart.append(points[i][3])
                    heartchart.append([distance,points[i][3]])
                    splitCurr = 0.0
            points.clear()
    else:
        activity.pace = round((activity.duration/60)/(activity.distance/1000),2)
        minutes = str(int(activity.pace)) 
        seconds = int((activity.pace%1)*60)
        seconds = format(seconds, '02d')
        activity.pace = minutes+':'+seconds
        activity.save()
    imglinks = []
    if activity.photo:
        imglinks.append(activity.photo.url)
    else:
        activity.photo = activity_type+"-default.jpg"
        activity.save()
        imglinks = activity.photo.url
    if activity.photo2:
        imglinks.append(activity.photo2.url)
    if activity.photo3:
        imglinks.append(activity.photo3.url)
    if request.method == 'POST':
        commenter = CommentForm(request.POST)
        if commenter.is_valid():
            body = commenter.cleaned_data['description']
#            return HttpResponse(body)
            createComment(request, str(activity.user),str(activity.permlink), str(body))
    else:
        commenter = CommentForm()

    context={
        "RUNNER": str(activity.user),
        "RUN_DATE": humanize.naturaldate(activity.start),
        "RUN_NOTES": activity.comments,
        "DISTANCE": str(activity.distance/1000),
        "HOURS": int(activity.duration/60/60),
        "MINUTES": int(activity.duration%3600/60),
        "SECONDS": activity.duration%60,
        "IMG_LINKS": imglinks,
        "RUN":activity,
        "SUBMITTED":activity.posted,
        "COMMENTS":comments,
        "COMCOUNT":len(comments),
        "COMMENTER":commenter,
        "RESPONDER":ReplyForm(),
        "REWARD":reward,
        "AVGPACE": avgpace,
        "HRMAX": maxHR,
        "HRAVG": avgHR,
        "ASCENT": ascent,
        'SPLIT_TIME': param, ## split time (y-axis, bar chart, normalized to min/km "6:01")
        'SPLITS': splits, ## split No. (x-axis, bar chart, "1,2,3,4...")
        'ELEV': elev, ## elevation data (y-axis, line chart, "25m")
        'PACE': pace, ## pace data (y-axis, line chart, min/km "5:50")
        'DIST': distance, ## distance list (x-axis, line chart, cumulative distance)
        'HEART': heart,
        'HEARTCHART': heartchart,
        'STEEM_ID': str(user),
        'SMOOTH': smoothDist,
        'ACTIVITY_TYPE': activity_type,
        }
    return HttpResponse(template.render(context,request))

@login_required
def create_post(request, activity_type, activity_id):
    steem_id = UserSocialAuth.objects.get(user=request.user)
    app = Client('exhaust.app',secret)
    if activity_type == 'run':
        activity = Run.objects.get(pk=activity_id)
    if activity_type == 'cycle':
        activity = Cycle.objects.get(pk=activity_id)
    if activity_type == 'hike':
        activity = Hike.objects.get(pk=activity_id)
    linktime = ('exhaust-'+str(activity_type)+'-'+str(int(activity.uploaded_at.timestamp())))
    body_text = ("I just finished a " + 
                str(activity.distance/1000) +
                "km " +
                activity_type +
                " that lasted about " +
                str(int(activity.duration/60/60)) +
                "hh:" + str(int(activity.duration%3600/60)) +
                "mm:" + str(activity.duration%60) +
                "ss !<br><center>![image](https://xhaust.me" + activity.photo.url + ")</center><br>" + activity.comments)
    if activity.photo2:
        body_text += "<br><center>![image](https://xhaust.me" + activity.photo2.url + ")</center><br>"
    if activity.photo3: 
        body_text += "<br><center>![image](https://xhaust.me" + activity.photo3.url + ")</center><br>"
    body_text += "<br>Check out some detailed info at [my EXHAUST page](https://xhaust.me/feed/"+str(activity_type)+"/"+str(activity_id)+"/)"+"<br>Join me in testing out [EXHAUST](https://xhaust.me)!"
#    body_text += "<br>### <center>Help me in testing out EXHAUST soon!</center>"
    comment = Comment(author=str(activity.user),
                      parent_permlink="exhaust",
                      permlink=str(linktime),
                      body=str(body_text),
                      title = str(activity.title),
                      json_metadata={
                          "app":"exhaust/0.2a",
                          "tags":[
                              "fitnation",
                              str(activity_type),
                          ]
                      })
    
    beneficiaries = [
        {'account': 'exhaust', 'weight': 2000},
    ]
    
    comment_options = CommentOptions(
        parent_comment=comment,
        allow_curation_rewards=True,
        extensions=[[0, {'beneficiaries': beneficiaries}]],
    )
    
    comment = comment.to_operation_structure()
    comment_options = comment_options.to_operation_structure()
    
    operations=[]
    operations.append(comment)
    operations.append(comment_options)
    
    app.access_token = steem_id.extra_data['access_token']
    app.broadcast(operations)
    activity.posted = True
    activity.permlink = '@' + str(activity.user) + '/' + str(linktime)
    activity.save()
    return redirect('/dash/' + activity_type + '/' +str(activity.pk) + '/')

def vote_activity(request, activity_type, activity_id):
    steem_id = UserSocialAuth.objects.get(user=request.user)
    app = Client('exhaust.app',secret)
    if activity_type == 'run':
        activity = Run.objects.get(pk=activity_id)
    if activity_type == 'cycle':
        activity = Cyle.objects.get(pk=activity_id)
    if activity_type == 'hike':
        activity = Hike.objects.get(pk=activity_id)
    link = activity.permlink
    permlink = re.sub('@'+str(activity.user)+'/','',link)
    vote = Vote(str(steem_id),str(activity.user),permlink,100)
    vote = vote.to_operation_structure()
    operations = []
    operations.append(vote)
    app.access_token = steem_id.extra_data['access_token']
    app.broadcast(operations)
    return redirect('/feed/' + str(activity_type) + '/' + str(activity_id) +'/')

def vote_comment(request, author, comment):
    steem_id = UserSocialAuth.objects.get(user=request.user)
    app = Client('exhaust.app',secret)
    vote = Vote(str(steem_id),str(author),comment,100)
    vote = vote.to_operation_structure()
    operations = []
    operations.append(vote)
    app.access_token = steem_id.extra_data['access_token']
    app.broadcast(operations)
    return redirect('/article/@'+str(author)+'/'+str(comment))

def vote_article(request, author, link):
    steem_id = UserSocialAuth.objects.get(user=request.user)
    app = Client('exhaust.app',secret)
    vote = Vote(str(steem_id),str(author),comment,100)
    vote = vote.to_operation_structure()
    operations = []
    operations.append(vote)
    app.access_token = steem_id.extra_data['access_token']
    app.broadcast(operations)
    return redirect('article/@'+str(author)+'/'+str(link))


def createComment(request, pauthor, plink, body):
    steem_id = UserSocialAuth.objects.get(user=request.user)
    app = Client('exhaust.app',secret)
    #permlink = 'comment-'+str(steem_id)+'-'+str(run.permlink)
    plink = re.sub('@.{1,16}/','',plink)
    json_data = {"app":"exhaust/0.1a"}
    body_text = body
    ppermlink = re.sub('@.{1,16}/','',plink)
    pauthor = pauthor
    permlink = (str(request.user)+'-comment-'+str(int(datetime.now().timestamp())))
    comment = Comment(author=str(request.user),
                      parent_permlink = str(plink),
                      parent_author = str(pauthor),
                      permlink=str(permlink),
                      body=str(body_text),
                      json_metadata=json_data)
    beneficiaries = [
        {'account': 'exhaust', 'weight': 500},
    ]
    comment_options = CommentOptions(
        parent_comment=comment,
        allow_curation_rewards=True,
        extensions=[[0, {'beneficiaries': beneficiaries}]],
    )
    comment = comment.to_operation_structure()
    comment_options = comment_options.to_operation_structure()
    operations=[]
    operations.append(comment)
    operations.append(comment_options)
    app.access_token = steem_id.extra_data['access_token']
    response = app.broadcast(operations)
    return redirect('/feed/')

def share_post(request, run_id):
    return HttpResponse("Hello -- You should be able to resteem posts with this button soon.")
