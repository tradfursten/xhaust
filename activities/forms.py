from django import forms
from django.contrib.auth.decorators import login_required
from markdownx.fields import MarkdownxFormField

from .models import Run

import floppyforms as floppy


class RunForm(forms.ModelForm):
    class Meta:
        model = Run
        fields = ('title', 'gpsfile', 'photo', 'distance', 'duration', 'comments')

class DatePicker(floppy.DateInput):  ##DATE WIDGET
    template_name = 'datepicker.html'

    class Media:
        js = (
            'js/jquery.min.js',
            'js/jquery-ui.min.js',
        )
        css = {
            'all': (
                'css/jquery-ui.css',
            )
        }

class TimePicker(forms.TimeInput):
    template_name = 'time.html'
    format='%H:%M:%S'


class Textarea(floppy.Textarea):
    template_name = 'textarea.html'
    rows = 5
    cols = 30


class Select(floppy.Select):
    template_name = 'select.html'


class DataForm(floppy.Form):
    title = forms.CharField(min_length=6, max_length=50, required=True)
    distance = forms.FloatField(min_value = 0.1, max_value=60.0, required=False)
    duration = floppy.CharField(max_length=10,required=False)
    time = floppy.TimeField(widget=floppy.TimeInput(format='%H:%M:%S'),required=False)
    date = forms.DateField(widget=DatePicker,required=False)

class PhotoForm(floppy.Form):
    photo1 = forms.ImageField(allow_empty_file=True, required=False)
    photo2 = forms.ImageField(allow_empty_file=True, required=False)
    photo3 = forms.ImageField(allow_empty_file=True, required=False)

class GPSForm(floppy.Form):
    gpsfile = forms.FileField(allow_empty_file=True, required=False)


class CommentForm(floppy.Form):
    description = floppy.CharField(widget=Textarea)

class ReplyForm(forms.Form):
    reply = forms.CharField(min_length=1, max_length=140, required=True)

class Slider(floppy.RangeInput):  ##SLIDER WIDGET
    min = 5
    max = 100
    step = 5
    template_name = 'slider.html'

    class Media:
        js = (
            'js/jquery.min.js',
            'js/jquery-ui.min.js',
        )
        css = {
            'all': (
                'css/jquery-ui.css',
            )
        }




class SlideForm(floppy.Form):
    weight = forms.IntegerField(widget=Slider)

    def clean_num(self):
        weight = self.cleaned_data['weight']
        if not 20 <= weight <= 100:
            raise forms.ValidationError("Must vote with at least 20% Power")

        if not weight % 5 == 0:
            raise forms.ValidationError("Enter a multiple of 5")
        return HttpResponse(weight)

class MarkdownForm(forms.Form):
    myfield = MarkdownxFormField()
