from activities.models import User, Run, Cycle, Hike
from datetime import datetime
from steem import Steem
import re

s = Steem()


def weekly_payout():
    account = s.get_account('exhaust')
    rewardstr = account["reward_sbd_balance"]
    rewards = re.sub(" SBD",'',rewardstr) 
    runList = Run.objects.filter(posted="True")
    now = datetime.now().timestamp()
    oneweek = now - 60*60*24*7
    twoweek = now - 60*60*24*7*2
    total_distance = 0.0
    userList = User.objects.all()
    s.claim_reward_balance(account='exhaust')
    for user in userList:
        user.pay_distance = 0.0
        user.save()
    for run in runList:
        if twoweek <= run.uploaded_at.timestamp() and run.uploaded_at.timestamp() <= oneweek:
            user = User.objects.get(username=str(run.user.username))
            total_distance += run.distance/1000
            user.pay_distance = float(user.pay_distance) + run.distance/1000
            user.save()
    userList = User.objects.all()
    for user in userList:
        if user.pay_distance > 0.0:
            shares = user.pay_distance / total_distance
            reward = round(shares*float(rewards),3)
            print(user.username + ": " + str(reward))
            message = "You have earned " + str(reward) + " SBD for getting EXHAUSTED last week! Keep it up!"
            s.transfer(user.username, amount = reward, asset='SBD', memo=message, account='exhaust')
