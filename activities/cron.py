from activities.models import User, Run, Cycle, Hike, Statistic
from datetime import datetime
from steem import Steem
import re


def weekly_payout():
    account = s.get_account('exhaust')
    rewardstr = account["reward_sbd_balance"]
    rewards = re.sub(" SBD",'',rewardstr) 
    runList = Run.objects.filter(posted="True")
    now = datetime.now().timestamp()
    oneweek = now - 60*60*24*7
    twoweek = now - 60*60*24*7*2
    total_distance = 0.0
    userList = User.objects.all()
    s.claim_reward_balance(account='exhaust')
    for user in userList:
        user.pay_distance = 0.0
        user.save()
    for run in runList:
        if twoweek <= run.uploaded_at.timestamp() and run.uploaded_at.timestamp() <= oneweek:
            user = User.objects.get(username=str(run.user.username))
            total_distance += run.distance/1000
            user.pay_distance = float(user.pay_distance) + run.distance/1000
            user.save()
    userList = User.objects.all()
    for user in userList:
        if user.pay_distance > 0.0:
            shares = user.pay_distance / total_distance
            reward = round(shares*float(rewards),3)
            print(user.username + ": " + str(reward))
            message = "You have earned " + str(reward) + " SBD for getting EXHAUSTED last week! Keep it up!"
            s.transfer(user.username, amount = reward, asset='SBD', memo=message, account='exhaust')

def update_leaderboard():

    runList = Run.objects.filter(posted="True")
    runList = list(reversed(runList))
    userList = User.objects.all()
    statlist = []
    statList = Statistic.objects.all()
    now = datetime.now().timestamp()
    oneday = 24*60*60
    sevenday = 7*24*60*60
    thirtyday = 30*24*60*60
    
    for username in statList:
        statlist.append(str(username.user))
    for user in userList:
        if user.username not in statlist:
            stat = Statistic(user = user)
            stat.save()
        else:
            stat = Statistic.objects.get(user=user)
            stat.activitycount7 = 0
            stat.activitycount30 = 0
            stat.rundistance7 = 0.0
            stat.rundistance30 = 0.0
            stat.runduration7 = 0.0
            stat.runduration30 = 0.0
            stat.runpace7 = 0.0
            stat.runpace30 = 0.0
            stat.bikedistance7 = 0.0
            stat.bikedistance30 = 0.0
            stat.bikespeed7 = 'null'
            stat.bikespeed30 = 'null'
            stat.save()
    for run in runList:
        runtime = (run.uploaded_at).timestamp()
        stat = Statistic.objects.get(user=run.user)
        if int(now - runtime) <= sevenday:
            stat.activitycount7 += 1
            stat.rundistance7 += round(run.distance/1000,2)
            stat.runduration7 += round(run.duration/60,2)
            stat.runpace7 = round((stat.runduration7)/(stat.rundistance7),2)
            stat.save()
        if int(now - runtime) <= thirtyday:
            stat.activitycount30 += 1
            stat.rundistance30 += run.distance/1000
            stat.runduration30 += run.duration/60
            stat.runpace30 = (stat.runduration30)/(stat.rundistance30)
            stat.save()
