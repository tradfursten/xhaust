import gpxpy
import gpxpy.gpx
import utm
import math
from datetime import datetime
import tcxparser
import re

point_list = []
lengthTotal = 0.0
runDuration = 0.0

#class dataScrape(self, filename):
#Import file function
#def importGPX(filename):
#    gpx_file = open(filename,'r')
#    gpx = gpxpy.parse(gpx_file)
#    #Load point list latitude and longitude from file
#    for track in gpx.tracks:
#        for segment in track.segments:
#            for point in segment.points:
#                timestamp = datetime.timestamp(point.time)
#                point_list.append([point.latitude,point.longitude,timestamp])
#    print(point_list[-1])
#    return(point_list)

def importGPX(filename):
    gpx_file = open(filename, 'r')
    gpx = gpxpy.parse(gpx_file)
    distance = gpx.length_2d()
    duration = gpx.get_duration()
    pace = round((duration/60)/(distance/1000),2)
    timeList = []
    splitList = []
    splitID = 1
    startTime = gpx.get_time_bounds().start_time.timestamp()
    for point in gpx.get_points_data():
        if point.distance_from_start >= splitID * 1000:
            timeList.append([splitID, (point.point.time.timestamp()-startTime)])
            splitID += 1
    splitList.append([timeList[0][0],timeList[0][1]/60])
    for i in range(1,len(timeList)):
        splitList.append([timeList[i][0],(timeList[i][1]-timeList[i-1][1])/60])
    return(distance, duration, pace, gpx.get_points_data(), gpx.get_time_bounds().start_time, splitList)

def importTCX(filename):
    tcx = tcxparser.TCXParser(filename)
    splitList = []
    counter = 1
    for lap in tcx.activity.Lap:
        for point in lap.Track.Trackpoint:
            try:
                pointElev = point.AltitudeMeters
            except AttributeError:
                pointElev = 0
            pointPos = point.DistanceMeters
            pointTime = point.Time
            try:
                pointHR = point.HeartRateBpm.Value
            except AttributeError:
                pointHR = 0
            try:
                pointTime = datetime.timestamp(datetime.strptime(str(pointTime),"%Y-%m-%dT%H:%M:%SZ"))
            except ValueError:
                pointTime = datetime.timestamp(datetime.strptime(str(pointTime),"%Y-%m-%dT%H:%M:%S.%fZ"))
            point_list.append([pointPos, pointTime, pointElev, pointHR,])
        distance = lap.DistanceMeters/1000
        duration = lap.TotalTimeSeconds/60
        pace = round(duration/distance,2)
        if len(tcx.activity.Lap) > 1:
            if lap.DistanceMeters >= 1:
                splitList.append([counter,pace])
                counter +=1
            else:
                splitList.append([round(distance,2),pace])
        else:
            prevtime = 0.0
            counter = 0
            finishtime = datetime.strptime(str(tcx.completed_at),'%Y-%m-%dT%H:%M:%SZ').timestamp()
            for i in range(1, int(distance) + 2):
                starttime = datetime.strptime(str(tcx.activity.Lap.Track.Trackpoint.Time), '%Y-%m-%dT%H:%M:%SZ').timestamp()
                for point in lap.Track.Trackpoint:
                    counter += 1
                    if int(point.DistanceMeters / 1000) >= i:
                        time = datetime.strptime(str(point.Time), '%Y-%m-%dT%H:%M:%SZ').timestamp()
                        delta = time-starttime
                        split = delta/60 - prevtime
                        splitList.append([i,split])
                        prevtime += delta/60 - prevtime
                        break
                    if point.Time == tcx.completed_at:
                        time = datetime.strptime(str(point.Time), '%Y-%m-%dT%H:%M:%SZ').timestamp()
                        delta = time-starttime
                        split = (delta/60 - prevtime)/((tcx.distance/1000)%1)
                        splitList.append([i,split])
                        prevtime += delta/60 - prevtime
                        break
    try:
        return(point_list, tcx.pace, tcx.hr_avg, tcx.hr_max, tcx.ascent, re.sub('T.{5,8}Z','',tcx.completed_at), tcx.distance, tcx.duration, splitList)
    except ZeroDivisionError:
        return(point_list, tcx.pace, 'NO HR DATA', 'NO HR DATA', tcx.ascent, re.sub('T.{5,8}Z','',tcx.completed_at), tcx.distance, tcx.duration, splitList)

#Convert points from Latitude and Longitude (DD.dddd) to UTM (E, N, ZONE, LETTER)
def convertUTM(points):
    for point in points:
        converted = utm.from_latlon(point[0],point[1])
        point[0], point[1] = converted[0], converted[1]

def runLength(points):
    lengthTotal = 0.0
    for i in range(1,len(points)):
        deltaX = abs(points[i][0] - points[i-1][0])
        deltaY = abs(points[i][1] - points[i-1][1])
        delta = math.sqrt((deltaX*deltaX)+(deltaY*deltaY))
        lengthTotal += delta
    lengthTotal = round(lengthTotal)
    return(lengthTotal)

def runTime(points):
    secondsDelta = points[-1][2] - points[0][2]
    return(secondsDelta)

def runPace(distance, duration, points):
    ### Tighten this shit up bigtime with datetime stuff. The below is fucking brutal
    # Pace in m/s
    pace = distance / duration
    # Pace in min/km
    minPace = 1000/pace/60
    secPace = round(1000/pace%60)
    pace = round(pace,2)
    minPace = round(minPace)
    print(str(minPace)+":"+str(secPace)+"/km")
    
#importGPX(filename)
#convertUTM(point_list)
#lengthTotal = runLength(point_list)
#runDuration = runTime(point_list)
#print(lengthTotal, runDuration)
#runPace(lengthTotal, runDuration, point_list)
