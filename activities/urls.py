from django.conf.urls import url, include
from django.urls import path

from jchart.views import ChartView

from markdownx import urls as markdownx

from . import charts
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('dash/', views.dash, name='dash'),
    path('new/<str:activity_type>', views.test_new_upload, name='upload'),
    path('feed/<str:activity_type>/<int:activity_id>/', views.gps_detail, name='detail-other'),
    path('feed/<str:activity_type>/<str:sort>/', views.feed, name='feed'),
    path('@<str:steem_id>/', views.browse, name='browse'),
    path('vote/@<str:author>/<str:comment>/', views.vote_comment, name='votec'),
    path('vote/<str:activity_type>/<int:activity_id>/', views.vote_activity, name='votep'),
#    path('vote/@<str:author>/<str:link>/', views.vote_article, name='votea'),
#    path('@<str:steem_id/<str:permlink>', views.user_comment, name='browse_comment'),
    path('leaderboard', views.leaderboard, name='leader'),
    path('dash/<str:activity_type>/<int:activity_id>/', views.gps_detail, name='detail-self'),
#    path('dash/cycle/<int:activity_id>/', views.gps_detail, name='detail-self'),
#    path('dash/hike/<int:activity_id>/', views.gps_detail, name='detail-self'),
    path('dash/<str:activity_type>/<int:activity_id>/post/', views.create_post, name='post'),
    path('comment/@<str:pauthor>/<str:plink>/', views.createComment, name='comment'),
    path('feed/run/<int:run_id>/share/', views.share_post, name='share'),
    path('article/@<str:steem_id>/<slug:link>', views.article, name='article'),
    path('test/', views.testfloppy, name='floppy'),
#    path('test/upload/', views.test_new_upload, name='upload_new'),
    path('test/<int:weight>/', views.testweight, name='testweight'),
    url(r'^signup/$', views.SignUpView.as_view(), name='signup'),
#    url(r'^ajax/calc_votevalue/$', views.calc_votevalue(), name='votecalc'),
]
