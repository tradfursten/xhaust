from django.db import models
# Create your models here.

class User(models.Model):
    username = models.CharField(max_length=25)
    nickname = models.CharField(max_length=25)
    join_date = models.DateTimeField('date joined')
    activitycountseven = models.IntegerField(default=0.0)
    rundistanceseven = models.FloatField(default=0.0)
    runpaceseven = models.FloatField(default=20.0)
    rundurationseven = models.FloatField(default=7200.0)
    pay_distance = models.FloatField(default=0.0)
    def __str__(self):
        return self.username
    
class Run(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=120, blank=False, default="Training Run")
    comments = models.TextField(max_length=2048, blank=True)
    gpsfile = models.FileField(upload_to='documents/runs/', blank=True)
    photo = models.ImageField(upload_to='documents/runs/', blank=True)
    photo2 = models.ImageField(upload_to='documents/runs/', blank=True)
    photo3 = models.ImageField(upload_to='documents/runs/', blank=True)
    uploaded_at = models.DateTimeField(auto_now_add=True)
    distance = models.IntegerField(default=0)  ##length in metres
    duration = models.IntegerField(default=0) ##time in seconds
    pace = models.CharField(max_length=10, blank=True)
    posted = models.BooleanField(default=False)
    permlink = models.CharField(max_length=128, blank=True)
    rewards = models.FloatField(default=0)
    start = models.DateTimeField(auto_now_add=True)
    verb = 'ran'
    def __str__(self):
        return (str(self.user) +": " +self.title + " - " + str(self.uploaded_at))

class Cycle(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=120, blank=False, default="Training Ride")
    comments = models.CharField(max_length=2048, blank=True)
    gpsfile = models.FileField(upload_to='documents/cycles/', blank=True)
    photo = models.ImageField(upload_to='documents/cycles/', blank=True)
    photo2 = models.ImageField(upload_to='documents/cycles/', blank=True)
    photo3 = models.ImageField(upload_to='documents/cycles/', blank=True)
    uploaded_at = models.DateTimeField(auto_now_add=True)
    distance = models.IntegerField(default=0)
    duration = models.IntegerField(default=0)
    speed = models.FloatField(default=0.0)
    posted = models.BooleanField(default=False)
    permlink = models.CharField(max_length=128, blank=True)
    start = models.DateTimeField(auto_now_add=True)
    verb = 'rode'
    def __str__(self):
        return (str(self.user) +": " +self.title + " - " + str(self.uploaded_at))

class Hike(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=120, blank=False, default="Rec Hike")
    comments = models.CharField(max_length=512)
    gpsfile = models.FileField(upload_to='documents/hikes/', blank=True)
    photo = models.ImageField(upload_to='documents/hikes/', blank=True)
    photo2 = models.ImageField(upload_to='documents/hikes/', blank=True)
    photo3 = models.ImageField(upload_to='documents/hikes/', blank=True)
    uploaded_at = models.DateTimeField(auto_now_add=True)
    distance = models.IntegerField(default=0)
    duration = models.IntegerField(default=0)
    climb_dist = models.IntegerField(default=0)
    climb_dur = models.IntegerField(default=0)
    descent_dist = models.IntegerField(default=0)
    descent_dur = models.IntegerField(default=0)
    posted = models.BooleanField(default=False)
    permlink = models.CharField(max_length=128, blank=True)
    start = models.DateTimeField(auto_now_add=True)
    verb = 'hiked'
    def __str__(self):
        return (str(self.user) +": " +self.title + " - " + str(self.uploaded_at))

class Statistic(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    activitycount7 = models.IntegerField(default=0)
    activitycount30 = models.IntegerField(default=0)
    runcount7 = models.IntegerField(default=0)
    bikecount7 = models.IntegerField(default=0)
    hikecount7 = models.IntegerField(default=0)
    rundistance7 = models.FloatField(default=0.0)
    rundistance30 = models.FloatField(default=0.0)
    runduration7 = models.FloatField(default=0.0)
    runduration30 = models.FloatField(default=0.0)
    runpace7 = models.FloatField(default=0.0)
    runpace30 = models.FloatField(default=0.0)
    bikedistance7 = models.FloatField(default=0.0)
    bikedistance30 = models.FloatField(default=0.0)
    bikespeed7 = models.CharField(default='null', max_length=5)
    bikespeed30 = models.CharField(default='null', max_length=5)
    hikedistance7 = models.FloatField(default=0.0)
    hikeclimb7 = models.FloatField(default=0.0)
    def __str__(self):
        return str(self.user)
