from .models import User, Run, Cycle, Hike

from datetime import datetime

import humanize
import json

def feedAll():
    runList = Run.objects.filter(posted=True)
    cycleList = Cycle.objects.filter(posted=True)
    hikeList = Hike.objects.filter(posted=True)

    runList = runList.order_by('uploaded_at')
    runList = runList.reverse()
    cycleList = cycleList.order_by('uploaded_at')
    cycleList = cycleList.reverse()
    hikeList = hikeList.order_by('uploaded_at')
    hikeList = hikeList.reverse()

    runCount = len(runList)
    cycleCount = len(cycleList)
    hikeCount = len(hikeList)

    runSlot = 0
    cycleSlot = 0
    hikeSlot = 0

    activityList = []
    activityList.clear()

    for i in range(0,runCount+cycleCount):
        adder = runList[runSlot]
        if (adder.uploaded_at.timestamp() < cycleList[cycleSlot].uploaded_at.timestamp()) and cycleSlot < cycleCount-1:
            adder = cycleList[cycleSlot]
            adder.type = 'cycle'
            if (adder.uploaded_at.timestamp() < hikeList[hikeSlot].uploaded_at.timestamp()) and hikeSlot < hikeCount-1:
                adder = hikeList[hikeSlot]
                adder.type = 'hike'
                hikeSlot +=1
            else:
                cycleSlot +=1
        else:
            runSlot +=1
            adder.type = 'run'

        activityList.append(adder)
#        activityList.reverse()
    return activityList
