# Generated by Django 2.1 on 2018-08-06 23:40

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='customusersocialauth',
            name='user',
        ),
        migrations.DeleteModel(
            name='CustomUserSocialAuth',
        ),
    ]
