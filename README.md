### INSTALL INSTRUCTIONS

#### Install Python dependencies
```
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt 
```

On a mac
```
brew install libmagic
```
#### Install GeoSpatial Libraries
```
#Fedora/Redhat
sudo dnf install bin-utils proj-devel gdal

#Ubuntu/Debian
sudo apt-get install binutils libproj-dev gdal-bin

#Mac
brew install gdal
```
#### Initialize Database
```
python3 manage.py makemigrations
python3 manage.py migrate
```
#### Update floppyforms widgets.py from:
```
context = self.get_context(name, value, attrs=attrs or {}, **kwargs)
```
to:
```
context = self.get_context(name, value, attrs=attrs or {})
```
#### Check BASE URL in SteemConnect backends.py (v2 is abandoned)
Change:
```
BASE_URL = 'https://v2.steemconnect.com'
```
to:
```
BASE_URL = 'https://steemconnect.com'
M
```

#### Create SuperUser for Django Project
```
python manage.py createsuperuser
# follow prompts
```

#### To start Django Server:
```
python manage.py runserver
````
* Open browser to localhost:8000
* To open admin page: localhost:8000/admin (login w/ username:password from *createsuperuser* step above)
