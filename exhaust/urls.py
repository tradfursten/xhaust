from django.conf.urls import url, include
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path

from app import views as app_views

urlpatterns = [
    path('', include('activities.urls')),
#    url(r'^$', app_views.home),
    url(r'^admin/', admin.site.urls),
    url(r'^login/$', app_views.home),
    url(r'^logout/$', app_views.logout),
    url(r'^done/$', app_views.done, name='done'),
    url(r'^markdownx/', include('markdownx.urls')),
    url(r'', include('social_django.urls'))
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
